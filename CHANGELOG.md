# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/).

## [Unreleased]

### Added
- conda-forge as installation source

### Fixed
- removed inappropriate print statement

## [2021.2.0]

### Added
- initial release
